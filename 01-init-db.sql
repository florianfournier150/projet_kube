-- Création de la base de données
CREATE DATABASE IF NOT EXISTS apicloud;

-- Utilisation de la base de données créée
USE apicloud;

-- Création de la table User
CREATE TABLE IF NOT EXISTS users (
    id INT AUTO_INCREMENT PRIMARY KEY,
    lastname VARCHAR(255) NOT NULL,
    firstname VARCHAR(255) NOT NULL,
    createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatedAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE TABLE IF NOT EXISTS products (
    id INT AUTO_INCREMENT PRIMARY KEY,
    libelle VARCHAR(255) NOT NULL,
    price DOUBLE NOT NULL,
    createdAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updatedAt TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);


-- Insérer des données de test
INSERT INTO users (lastname, firstname) VALUES
('John', 'Doe'),
('Jane', 'Smith');

INSERT INTO products (libelle, price) VALUES
('Chaussure', 12.69),
('Chaussette', 42.69);