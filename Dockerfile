FROM node:18-alpine

WORKDIR /tp

COPY . .

RUN npm install

EXPOSE 3000

CMD ["npm", "start"]