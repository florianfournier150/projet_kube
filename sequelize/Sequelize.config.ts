const sequelizeConfig = {
  development: {
    database: 'apicloud',
    username: 'root',
    password: 'root_password',
    host: 'localhost',
    dialect: 'mysql',
    port: 30003
  },
  production: {
    // Configuration pour la production
  },
};

export default sequelizeConfig;