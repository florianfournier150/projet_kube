import { Model, DataTypes } from 'sequelize';
import sequelize from '../sequelize/Sequelize.service';

class User extends Model {
    public id!: string;
    public firstname!: string;
    public lastname!: string;
}

User.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    firstname: {
      type: DataTypes.STRING,
      allowNull: false
    },
    lastname: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'user'
  });
  
export default User;