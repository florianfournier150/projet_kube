import { Model, DataTypes } from 'sequelize';
import sequelize from '../sequelize/Sequelize.service';

class Product extends Model {
    public id!: string;
    public libelle!: string;
    public price!: number;
}

Product.init({
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },
    libelle: {
      type: DataTypes.STRING,
      allowNull: false
    },
    price: {
      type: DataTypes.NUMBER,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'product'
  });
  
export default Product;