import axios from "axios";
import express, {Request, Response} from "express";
import Product from "./models/Products";
import User from "./models/User";
const app = express();
app.use(express.json());
const PORT = 3000;

app.get("/status", (request, response) => {
  response.status(200);
  response.send("ok");
});

app.get("/error", (request, response) => {
  response.status(500);
  response.send("NOT ok");
});

// File: maxCpuUsage.js

app.get("/cpu", async (request, response) => {
  const ok = Date.now() + 1000 * 60;
  while (Date.now() < ok) {
    console.log(Math.sqrt(Date.now()));
  }
  response.status(200);
  response.send("ok");
});

app.get("/exit", (request, response) => {
  process.exit(0);
});

app.listen(PORT, () => {
  console.log("Server Listening on PORT:", PORT);
});

app.get("/user/:id", async (req:Request, res:Response) => {
  const {id} = req.params;
  res.status(200).json(await User.findByPk(id));
})

app.get("/user", async (req:Request, res:Response) => {
  const {offset,limit} = req.query;
  if(typeof offset == 'number' && typeof limit == 'number') {
    res.status(200).json(await User.findAll({
      order: [['createdAt', 'DESC']], 
      limit: limit,
      offset: offset
    }));
  } else {
    res.status(200).json(await User.findAll({
      order: [['createdAt', 'DESC']], 
      limit: 10,
      offset: 0
    }));
  }
})

app.get("/lastuser", async (req:Request, res:Response) => {
  res.status(200).json(await User.findOne({
    order: [['id', 'DESC']],
  }))
})

interface FormData {
  firstname: string;
  lastname: string;
}

app.post('/user', async (req: Request<{}, {}, FormData>, res: Response) => {
  const { firstname, lastname } = req.body;
  const newUser = await User.create({firstname:firstname, lastname:lastname});
  if(!newUser)res.status(500);
  axios.get("/lastproduct") 
    .then(async (response) => {
      if(response.status === 200){
        const newProduct = await Product.create({libelle:response.data.libelle, price:response.data.price})
        res.status(200).send("OK")
      }
    })
    .catch((error:any) => {
      res.status(400).json(error.message)
    })
})